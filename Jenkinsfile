pipeline {
    agent any
    tools {
        // tools names here need to exist in tools section with the same name in jenkins config
        maven 'Maven 3.0.5'
        jdk 'jdk8'
    }
	environment {
        // This can be nexus3 or nexus2
        NEXUS_VERSION = "nexus3"
        // This can be http or https
        NEXUS_PROTOCOL = "http"
        // Where your Nexus is running
        NEXUS_URL = "www.nexus-training.link/"
        // Repository where we will upload the artifact
        NEXUS_REPOSITORY = "maven-repo"
        // Jenkins credential id to authenticate to Nexus OSS
        NEXUS_CREDENTIAL_ID = "nexus-cred"
    }
    stages {
         stage ('Build') {
            steps {
            // you need to install pipeline-utility-steps plugin first 
	      script {
            def pom = readMavenPom file: 'pom.xml'
      
	    sh "mvn -B versions:set -DnewVersion=${pom.version}-${BUILD_NUMBER}"   
               }
                     sh "mvn -Dmaven.test.skip=true clean package"
			}
        }
		stage ('Unit Test') {
            steps {
			    sh "mvn -B clean test"
            }
        }
		stage ('Integ Test') {
            steps {
			    sh "mvn -B clean verify -Dsurefire.skip=true"
            }
        }
		stage ('Send email') {
		   steps {
             emailext ( subject: "Approve Deployment", body: "Approve build with link $BUILD_URL ",  to: "samar21elsayed@gmail.com" )
           }
		}
        stage ('Approval') {
		    options {
			timeout(time:3, unit:'DAYS')
			}
			steps {
			  script {
                  env.reply = input  message:  "Approve Deployment?"  , ok: "Yes",  parameters: [choice(name: 'RELEASE_SCOPE', choices:     'patch\nminor\nmajor', description: 'What is the release scope?')]
			   }
			   sh "echo ${env.reply}"
			   }
}
        stage ('Artifact Upload') {
			steps {
                script {
                    // Read POM xml file using 'readMavenPom' step , this step 'readMavenPom' is included in: https://plugins.jenkins.io/pipeline-utility-steps
                    pom = readMavenPom file: "pom.xml";
                    // Find built artifact under target folder
                    filesByGlob = findFiles(glob: "target/*.${pom.packaging}");
                    // Print some info from the artifact found
                    echo "{filesByGlob}"
                    echo "${filesByGlob[0].name} ${filesByGlob[0].path} ${filesByGlob[0].directory} ${filesByGlob[0].length} ${filesByGlob[0].lastModified}"
                    // Extract the path from the File found
                    artifactPath = filesByGlob[0].path;
                    // Assign to a boolean response verifying If the artifact name exists
                    artifactExists = fileExists artifactPath;

                    if(artifactExists) {
                        echo "*** File: ${artifactPath}, group: ${pom.groupId}, packaging: ${pom.packaging}, version ${pom.version}";

                        nexusArtifactUploader(
                            nexusVersion: NEXUS_VERSION,
                            protocol: NEXUS_PROTOCOL,
                            nexusUrl: NEXUS_URL,
                            groupId: pom.groupId,
                            version: pom.version,
                            repository: NEXUS_REPOSITORY,
                            credentialsId: NEXUS_CREDENTIAL_ID,
                            artifacts: [
                                // Artifact generated such as .jar, .ear and .war files.
                                [artifactId: pom.artifactId,
                                classifier: '',
                                file: artifactPath,
                                type: pom.packaging],

                                // Lets upload the pom.xml file for additional information for Transitive dependencies
//                                [artifactId: pom.artifactId,
//                                classifier: '',
//                                file: "pom.xml",
//                                type: "pom"]
                            ]
                        );

                    } else {
                        error "*** File: ${artifactPath}, could not be found";
                    }
                }
            }
}
        stage ('Artifact Upload Result') {
			steps {
			  script {
                  def logz = currentBuild.rawBuild.getLog(10000);
                  def result = logz.find { it.contains('Failed to deploy artifacts') }
                  if (result) 
                  {
                     error (result)
                  }     
			   }
			   }
}
        stage ('Deployment') {
		    steps {
               sh "whoami" 
		       sh "sudo pwd"
		       sh "sudo rm -f  /etc/ansible/builds/*.jar"
			   sh "sudo cp /var/lib/jenkins/workspace/maven-adder/target/*.jar  /etc/ansible/builds/"
               sh "sudo ansible-playbook /etc/ansible/docker.yml"
			 }
		   }
}

    post {
		always {
			emailext ( body: "<br>Project: ${env.JOB_NAME} <br>Build Number: ${env.BUILD_NUMBER} <br> URL of build: ${env.BUILD_URL}" ,  mimeType: 'text/html' , subject: "${currentBuild.result} CI: Project name -> ${env.JOB_NAME}", to: "samar21elsayed@gmail.com" )  
		 }
	   }
}
