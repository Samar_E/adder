A simple CI/CD pipeline that is run by jenkins to : 

- Build Adder.jar using Maven.
- Send Manual Approval email
- Upload artifacts to Nexus Repo.
- Deploy on a server using ansible playbook to create a docker image from the jar then run it as a container.
- Send an email with the pipline status

Infrastructure/resources used :
- EC2 instances
- ALB 
- Route 53 
